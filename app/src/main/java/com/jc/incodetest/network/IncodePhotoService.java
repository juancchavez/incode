package com.jc.incodetest.network;

import com.jc.incodetest.model.Photo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by JuanCarlos on 3/12/17.
 */

public interface IncodePhotoService {
    @GET("/api/photo")
    Call<List<Photo>> getPhotos();
}
