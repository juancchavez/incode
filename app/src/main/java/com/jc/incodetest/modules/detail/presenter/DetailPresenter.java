package com.jc.incodetest.modules.detail.presenter;


import com.jc.incodetest.model.Photo;
import com.jc.incodetest.modules.detail.DetailContract;

/**
 * Created by JC on 3/16/17.
 */

public class DetailPresenter implements DetailContract.Presenter {

    private DetailContract.View mView;
    private DetailContract.LocalClient mLocalClient;
    private Photo mPhoto;

    public DetailPresenter(DetailContract.LocalClient localClient) {
        mLocalClient = localClient;
    }

    /**
     * DetailContract.Presenter methods
     */
    @Override
    public void onViewAttached(DetailContract.View view) {
        mView = view;
    }

    @Override
    public void onViewDetached() {
        mView = null;
    }

    @Override
    public void onDestroyed() {
        mLocalClient.onDestroy();
    }

    @Override
    public void getPhoto(int id) {
        if(mPhoto == null) {
            mPhoto = mLocalClient.getPhoto(id);
        }
        mView.displayPhoto(mPhoto);
    }

    @Override
    public void onShareClicked() {
        mView.sharePhoto(mPhoto);
    }
}
