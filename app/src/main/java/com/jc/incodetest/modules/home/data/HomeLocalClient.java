package com.jc.incodetest.modules.home.data;

import com.jc.incodetest.db.RealmManagerContract;
import com.jc.incodetest.model.Photo;
import com.jc.incodetest.modules.home.HomeContract;

import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;

/**
 * Created by JC on 3/15/17.
 */

public class HomeLocalClient implements HomeContract.LocalClient {

    private RealmManagerContract mRealmManager;
    private Realm mRealm;

    @Inject
    public HomeLocalClient(RealmManagerContract realmManager) {
        mRealmManager = realmManager;
        mRealm = realmManager.getRealmInstance();
    }

    @Override
    public void savePhotos(List<Photo> photos, final HomeContract.LocalClientCallback<Void> callback) {
        mRealmManager.savePhotos(mRealm, photos,
                new Realm.Transaction.OnSuccess() {
                    @Override
                    public void onSuccess() {
                        callback.onSuccess(null);
                    }
                }, new Realm.Transaction.OnError() {
                    @Override
                    public void onError(Throwable error) {
                        callback.onError(error);
                    }
                });
    }

    @Override
    public void savePhoto(Photo photo, final HomeContract.LocalClientCallback<Void> callback) {
        mRealmManager.savePhoto(mRealm, photo,
                new Realm.Transaction.OnSuccess() {
                    @Override
                    public void onSuccess() {
                        callback.onSuccess(null);
                    }
                }, new Realm.Transaction.OnError() {
                    @Override
                    public void onError(Throwable error) {
                        callback.onError(error);
                    }
                });
    }

    @Override
    public List<Photo> getPhotos() {
        return mRealmManager.getPhotos(mRealm);
    }

    @Override
    public void onDestroy() {
        mRealmManager.closeRealm(mRealm);
    }
}
