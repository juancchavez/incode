package com.jc.incodetest.modules.home.view;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.jc.incodetest.IncodeApplication;
import com.jc.incodetest.R;
import com.jc.incodetest.model.Photo;
import com.jc.incodetest.modules.core.BasePresenterActivity;
import com.jc.incodetest.modules.core.PresenterFactory;
import com.jc.incodetest.modules.detail.view.DetailActivity;
import com.jc.incodetest.modules.home.DaggerHomeComponent;
import com.jc.incodetest.modules.home.HomeContract;
import com.jc.incodetest.modules.home.HomeModule;
import com.jc.incodetest.modules.home.view.adapter.PhotosAdapter;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by JuanCarlos on 3/12/17.
 */

public class HomeActivity extends BasePresenterActivity<HomeContract.Presenter, HomeContract.View>
        implements HomeContract.View {

    private static final int TAKE_PHOTO_REQUEST = 101;
    private static final int LOADER_ID = 101;

    private HomeContract.Presenter mPresenter;
    private String mCurrentPhotoPath;

    @Inject
    PresenterFactory<HomeContract.Presenter> mPresenterFactory;

    @Inject
    PhotosAdapter mPhotosAdapter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        DaggerHomeComponent.builder()
                .homeModule(new HomeModule(this))
                .applicationComponent(IncodeApplication.getInstance().getComponent())
                .build()
                .injectHomeActivity(this);

        initView();
        initLoader(101);
    }

    @Override
    protected void onStop() {
        mPresenter.onViewDetached();
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.item_camera) {
            mPresenter.onCameraClicked();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TAKE_PHOTO_REQUEST && resultCode == RESULT_OK) {
            mPresenter.onPictureTaken(mCurrentPhotoPath);
        }
    }

    /**
     * BasePresenterActivity methods
     */
    @NonNull
    @Override
    protected String tag() {
        return HomeActivity.class.getCanonicalName();
    }

    @NonNull
    @Override
    protected PresenterFactory<HomeContract.Presenter> getPresenterFactory() {
        return mPresenterFactory;
    }

    @Override
    protected void onPresenterPrepared(@NonNull HomeContract.Presenter presenter) {
        mPresenter = presenter;
        mPresenter.onViewAttached(this);
        mPresenter.initialize();
    }

    @Override
    protected void onPresenterDestroyed() {
        mPresenter.onDestroyed();
    }

    /**
     * HomeContract.View methods
     */
    @Override
    public void displayPhotos(List<Photo> photos) {
        mPhotosAdapter.addItems(photos);
    }

    @Override
    public void launchDetailsModule(int photoId) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra("id", photoId);
        startActivity(intent);
    }

    @Override
    public void launchCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            photoFile = createImageFile();

            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.jc.incodetest.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, TAKE_PHOTO_REQUEST);
            }
        }
    }

    @Override
    public void addPhotoToList(Photo photo) {
        mPhotosAdapter.addItem(photo);
        recyclerview.scrollToPosition(0);
    }

    /**
     * private methods
     */
    public void initView() {
        setSupportActionBar(toolbar);

        int spanCount = 2;
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            spanCount = 3;
        }

        RecyclerView.LayoutManager manager = new GridLayoutManager(this, spanCount);

        mPhotosAdapter.setOnItemClickListener(new PhotosAdapter.OnItemClickListener<Photo>() {
            @Override
            public void onItemClicked(Photo item) {
                mPresenter.onPhotoSelected(item);
            }
        });
        recyclerview.setAdapter(mPhotosAdapter);
        recyclerview.setLayoutManager(manager);
    }

    private File createImageFile() {
        File image = null;
        try {
            // Create an image file name
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
            String imageFileName = getString(R.string.photo_file_prefix) + timeStamp + "_";
            File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );

            // Save a file: path for use with ACTION_VIEW intents
            mCurrentPhotoPath = image.getAbsolutePath();
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        return image;
    }

}
