package com.jc.incodetest.modules.home;

import com.jc.incodetest.modules.core.PresenterFactory;
import com.jc.incodetest.modules.home.data.HomeApiClient;
import com.jc.incodetest.modules.home.data.HomeLocalClient;
import com.jc.incodetest.modules.home.presenter.HomePresenterFactory;
import com.jc.incodetest.modules.home.view.HomeActivity;
import com.jc.incodetest.modules.home.view.adapter.PhotosAdapter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by JuanCarlos on 3/12/17.
 */
@Module
public class HomeModule {
    private final HomeActivity homeActivity;

    public HomeModule(HomeActivity homeActivity) {
        this.homeActivity = homeActivity;
    }

    @Provides
    @HomeScope
    public HomeActivity homeActivity() {
        return homeActivity;
    }

    @Provides
    @HomeScope
    public PhotosAdapter photosAdapter(HomeActivity homeActivity) {
        return new PhotosAdapter(homeActivity);
    }

    @Provides
    @HomeScope
    public PresenterFactory<HomeContract.Presenter> presenterFactory(HomePresenterFactory homePresenterFactory) {
        return homePresenterFactory;
    }

    @Provides
    @HomeScope
    public HomeContract.ApiClient apiClient(HomeApiClient homeApiClient) {
        return homeApiClient;
    }

    @Provides
    @HomeScope
    public HomeContract.LocalClient localClient(HomeLocalClient homeLocalClient) {
        return homeLocalClient;
    }
}
