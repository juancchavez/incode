package com.jc.incodetest.modules.home.view.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.jc.incodetest.R;
import com.jc.incodetest.model.Photo;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by JC on 3/15/17.
 */

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.PhotosViewHolder> {

    private List<Photo> mPhotos;
    private OnItemClickListener<Photo> mOnItemClickListener;
    private LayoutInflater mInflater;
    private Context mContext;

    @Inject
    public PhotosAdapter(Context context) {
        mPhotos = new ArrayList<>();
        mInflater = LayoutInflater.from(context);
        mContext = context;
    }

    @Override
    public PhotosAdapter.PhotosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mInflater.inflate(R.layout.item_photo, parent, false);
        return new PhotosViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PhotosAdapter.PhotosViewHolder holder, int position) {
        Photo photo = mPhotos.get(position);
        holder.bind(photo);
    }

    @Override
    public int getItemCount() {
        return mPhotos.size();
    }

    public void addItems(List<Photo> photos) {
        mPhotos.addAll(photos);
        notifyDataSetChanged();
    }

    public void addItem(Photo photo) {
        mPhotos.add(0, photo);
        notifyItemInserted(0);
    }

    public void setOnItemClickListener(OnItemClickListener<Photo> listener) {
        mOnItemClickListener = listener;
    }

    public interface OnItemClickListener<V> {
        void onItemClicked(V item);
    }

    class PhotosViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        Photo photo;

        @BindView(R.id.imgv_photo)
        SimpleDraweeView imgvPhoto;

        public PhotosViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        public void bind(Photo photo) {
            this.photo = photo;

            if(photo.getPhoto().startsWith("file")) {
                int size = (int) mContext.getResources().getDimension(R.dimen.photo_item_height);
                ImageRequest request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(photo.getPhoto()))
                        .setResizeOptions(new ResizeOptions(size, size))
                        .build();
                DraweeController controller = Fresco.newDraweeControllerBuilder()
                        .setOldController(imgvPhoto.getController())
                        .setImageRequest(request)
                        .build();

                imgvPhoto.setController(controller);
            } else {
                imgvPhoto.setImageURI(photo.getPhoto());
            }
        }

        @Override
        public void onClick(View view) {
            if(mOnItemClickListener != null)
                mOnItemClickListener.onItemClicked(photo);
        }
    }
}
