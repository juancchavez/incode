package com.jc.incodetest.modules.home;

import android.net.Uri;

import com.jc.incodetest.model.Photo;

import java.util.List;

/**
 * Created by JC on 3/15/17.
 */

public class HomeContract {
    public interface View {
        void displayPhotos(List<Photo> photos);
        void launchDetailsModule(int photoId);
        void launchCamera();
        void addPhotoToList(Photo photo);
    }

    public interface Presenter extends com.jc.incodetest.modules.core.Presenter<View> {
        void initialize();
        void onPhotoSelected(Photo photo);
        void onCameraClicked();
        void onPictureTaken(String path);
    }

    public interface ApiClient {
        void getPhotos(ApiClientCallback<List<Photo>> callback);
        void onDestroy();
    }

    public interface LocalClient {
        void savePhotos(List<Photo> photos, LocalClientCallback<Void> callback);
        void savePhoto(Photo photo, LocalClientCallback<Void> callback);
        List<Photo> getPhotos();
        void onDestroy();
    }

    public interface ApiClientCallback<T> {
        void onSuccess(T response);
        void onError(Throwable t);
    }

    public interface LocalClientCallback<T> {
        void onSuccess(T response);
        void onError(Throwable t);
    }
}
