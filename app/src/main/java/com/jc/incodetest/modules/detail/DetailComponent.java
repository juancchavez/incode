package com.jc.incodetest.modules.detail;

import com.jc.incodetest.ApplicationComponent;
import com.jc.incodetest.modules.detail.data.DetailLocalClient;
import com.jc.incodetest.modules.detail.view.DetailActivity;

import dagger.Component;

/**
 * Created by JuanCarlos on 3/12/17.
 */
@DetailScope
@Component(modules = DetailModule.class, dependencies = ApplicationComponent.class)
public interface DetailComponent {
    void injectDetailActivity(DetailActivity detailActivity);
    DetailLocalClient detailLocalClient();
}
