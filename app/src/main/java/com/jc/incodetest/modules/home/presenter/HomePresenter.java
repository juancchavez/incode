package com.jc.incodetest.modules.home.presenter;

import android.net.Uri;
import android.util.Log;

import com.jc.incodetest.model.Photo;
import com.jc.incodetest.modules.home.HomeContract;

import java.io.File;
import java.util.List;

/**
 * Created by JC on 3/15/17.
 */

public class HomePresenter implements HomeContract.Presenter {

    HomeContract.View mView;
    HomeContract.ApiClient mApiClient;
    HomeContract.LocalClient mLocalClient;

    HomePresenter(HomeContract.ApiClient apiClient, HomeContract.LocalClient localClient) {
        mApiClient = apiClient;
        mLocalClient = localClient;
    }

    /**
     * HomeContract.Presenter methods
     */
    @Override
    public void onViewAttached(HomeContract.View view) {
        mView = view;
    }

    @Override
    public void onViewDetached() {
        mView = null;
    }

    @Override
    public void onDestroyed() {
        mApiClient.onDestroy();
        mLocalClient.onDestroy();
    }

    @Override
    public void initialize() {
        List<Photo> photos = mLocalClient.getPhotos();
        if(photos != null && !photos.isEmpty()) {
            mView.displayPhotos(photos);
        } else {
            getPhotosFromApi();
        }
    }

    @Override
    public void onPhotoSelected(Photo photo) {
        mView.launchDetailsModule(photo.getId());
    }

    @Override
    public void onCameraClicked() {
        mView.launchCamera();
    }

    @Override
    public void onPictureTaken(String path) {
        File f = new File(path);
        Uri imageUri = Uri.fromFile(f);
        final Photo photo = new Photo(true, imageUri.toString());
        mLocalClient.savePhoto(photo, new HomeContract.LocalClientCallback<Void>() {
            @Override
            public void onSuccess(Void response) {
                mView.addPhotoToList(photo);
            }

            @Override
            public void onError(Throwable t) {
                Log.e(HomePresenter.class.getCanonicalName(), t.getMessage());
            }
        });
    }

    /**
     * private methods
     */
    private void processApiResponse(List<Photo> photos) {
        mLocalClient.savePhotos(photos, new HomeContract.LocalClientCallback<Void>() {
            @Override
            public void onSuccess(Void response) {
                List<Photo> photos = mLocalClient.getPhotos();
                if(photos != null && !photos.isEmpty()) {
                    mView.displayPhotos(photos);
                }
            }

            @Override
            public void onError(Throwable t) {
                Log.e(HomePresenter.class.getCanonicalName(), t.getMessage());
            }
        });
    }

    private void getPhotosFromApi() {
        mApiClient.getPhotos(new HomeContract.ApiClientCallback<List<Photo>>() {
            @Override
            public void onSuccess(List<Photo> response) {
                processApiResponse(response);
            }

            @Override
            public void onError(Throwable t) {
                Log.e(HomePresenter.class.getCanonicalName(), t.getMessage());
            }
        });
    }
}
