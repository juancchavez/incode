package com.jc.incodetest.modules.home.presenter;

import com.jc.incodetest.modules.core.PresenterFactory;
import com.jc.incodetest.modules.home.HomeContract;

import javax.inject.Inject;

/**
 * Created by JC on 3/15/17.
 */

public class HomePresenterFactory implements PresenterFactory<HomeContract.Presenter> {
    private HomeContract.ApiClient mApiClient;
    private HomeContract.LocalClient mLocalClient;

    @Inject
    public HomePresenterFactory(HomeContract.ApiClient apiClient, HomeContract.LocalClient localClient) {
        mApiClient = apiClient;
        mLocalClient = localClient;
    }

    @Override
    public HomeContract.Presenter create() {
        return new HomePresenter(mApiClient, mLocalClient);
    }
}
