package com.jc.incodetest.modules.detail;

import com.jc.incodetest.modules.core.PresenterFactory;
import com.jc.incodetest.modules.detail.data.DetailLocalClient;
import com.jc.incodetest.modules.detail.presenter.DetailPresenterFactory;
import com.jc.incodetest.modules.detail.view.DetailActivity;
import com.jc.incodetest.modules.home.HomeContract;

import dagger.Module;
import dagger.Provides;

/**
 * Created by JuanCarlos on 3/12/17.
 */
@Module
public class DetailModule {
    private final DetailActivity detailActivity;

    public DetailModule(DetailActivity detailActivity) {
        this.detailActivity = detailActivity;
    }

    @Provides
    @DetailScope
    public DetailActivity detailActivity() {
        return detailActivity;
    }

    @Provides
    @DetailScope
    public PresenterFactory<DetailContract.Presenter> presenterFactory(DetailPresenterFactory detailPresenterFactory) {
        return detailPresenterFactory;
    }

    @Provides
    @DetailScope
    public DetailContract.LocalClient localClient(DetailLocalClient detailLocalClient) {
        return detailLocalClient;
    }
}
