package com.jc.incodetest.modules.detail.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.facebook.common.executors.CallerThreadExecutor;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.jc.incodetest.IncodeApplication;
import com.jc.incodetest.R;
import com.jc.incodetest.model.Photo;
import com.jc.incodetest.modules.core.BasePresenterActivity;
import com.jc.incodetest.modules.core.PresenterFactory;
import com.jc.incodetest.modules.detail.DaggerDetailComponent;
import com.jc.incodetest.modules.detail.DetailContract;
import com.jc.incodetest.modules.detail.DetailModule;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by JuanCarlos on 3/12/17.
 */

public class DetailActivity extends BasePresenterActivity<DetailContract.Presenter, DetailContract.View>
        implements DetailContract.View {

    private static final int LOADER_ID = 102;
    private int mPhotoId;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imgv_photo)
    SimpleDraweeView imgvPhoto;
    @BindView(R.id.txtv_description)
    TextView txtvDescription;

    private DetailContract.Presenter mPresenter;

    @Inject
    PresenterFactory<DetailContract.Presenter> mPresenterFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        DaggerDetailComponent.builder()
                .detailModule(new DetailModule(this))
                .applicationComponent(IncodeApplication.getInstance().getComponent())
                .build()
                .injectDetailActivity(this);

        mPhotoId = getIntent().getIntExtra("id", 0);
        initLoader(LOADER_ID);
        initView();
    }

    @Override
    protected void onPresenterDestroyed() {
        mPresenter.onDestroyed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.details_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.item_share) {
            mPresenter.onShareClicked();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * BasePresenterActivity methods
     */
    @NonNull
    @Override
    protected String tag() {
        return DetailActivity.class.getCanonicalName();
    }

    @NonNull
    @Override
    protected PresenterFactory<DetailContract.Presenter> getPresenterFactory() {
        return mPresenterFactory;
    }

    @Override
    protected void onPresenterPrepared(@NonNull DetailContract.Presenter presenter) {
        mPresenter = presenter;
        mPresenter.onViewAttached(this);
        mPresenter.getPhoto(mPhotoId);
    }

    /**
     * DetailContract.View methods
     */
    @Override
    public void displayPhoto(Photo photo) {
        getSupportActionBar().setTitle(photo.getTitle());
        imgvPhoto.setImageURI(photo.getPhoto());

        if(photo.getComment() != null && !photo.getComment().isEmpty()) {
            txtvDescription.setText(photo.getComment());
        } else {
            txtvDescription.setVisibility(View.GONE);
        }
    }

    @Override
    public void sharePhoto(Photo photo) {
        if(photo.isLocalFile()) {
            launchShareChooser(Uri.parse(photo.getPhoto()));
        } else {
            // To get image using Fresco
            ImageRequest imageRequest = ImageRequestBuilder
                    .newBuilderWithSource( Uri.parse(photo.getPhoto()))
                    .setProgressiveRenderingEnabled(true)
                    .build();

            ImagePipeline imagePipeline = Fresco.getImagePipeline();
            DataSource<CloseableReference<CloseableImage>> dataSource =
                    imagePipeline.fetchDecodedImage(imageRequest, this);

            dataSource.subscribe(new BaseBitmapDataSubscriber() {

                @Override
                public void onNewResultImpl(@Nullable Bitmap bitmap) {
                    launchShareChooser(getLocalBitmapUri(bitmap));
                }

                @Override
                public void onFailureImpl(DataSource dataSource) {

                }

            }, CallerThreadExecutor.getInstance());
        }
    }

    /**
     * private methods
     */
    private void initView() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    private void launchShareChooser(Uri uri) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("image/jpg");
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(shareIntent, "Share image using"));
    }

    private Uri getLocalBitmapUri(Bitmap bmp) {
        Uri bmpUri = null;
        try {
            File file =  new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }
}
