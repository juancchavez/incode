package com.jc.incodetest.modules.home.data;

import com.jc.incodetest.model.Photo;
import com.jc.incodetest.modules.home.HomeContract;
import com.jc.incodetest.network.IncodePhotoService;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by JC on 3/15/17.
 */

public class HomeApiClient implements HomeContract.ApiClient {

    private IncodePhotoService mPhotoService;
    private Call<List<Photo>> mPhotoCall;

    @Inject
    public HomeApiClient(IncodePhotoService service) {
        mPhotoService = service;
    }

    /**
     * HomeContract.ApiClient methods
     */
    @Override
    public void getPhotos(final HomeContract.ApiClientCallback<List<Photo>> callback) {
        mPhotoCall = mPhotoService.getPhotos();
        mPhotoCall.enqueue(new Callback<List<Photo>>() {
            @Override
            public void onResponse(Call<List<Photo>> call, Response<List<Photo>> response) {
                callback.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<List<Photo>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onDestroy() {
        if(mPhotoCall != null)
            mPhotoCall.cancel();
    }

}
