package com.jc.incodetest.modules.core;

/**
 * Created by JC on 3/15/17.
 */

public interface Presenter<V> {
    void onViewAttached(V view);
    void onViewDetached();
    void onDestroyed();
}
