package com.jc.incodetest.modules.detail.presenter;

import com.jc.incodetest.modules.core.PresenterFactory;
import com.jc.incodetest.modules.detail.DetailContract;

import javax.inject.Inject;

/**
 * Created by JC on 3/16/17.
 */

public class DetailPresenterFactory implements PresenterFactory<DetailContract.Presenter> {

    private DetailContract.LocalClient mLocalClient;

    @Inject
    public DetailPresenterFactory(DetailContract.LocalClient localClient) {
        mLocalClient = localClient;
    }

    @Override
    public DetailContract.Presenter create() {
        return new DetailPresenter(mLocalClient);
    }
}
