package com.jc.incodetest.modules.detail;

import android.net.Uri;

import com.jc.incodetest.model.Photo;

/**
 * Created by JC on 3/16/17.
 */

public class DetailContract {

    public interface View {
        void displayPhoto(Photo photo);
        void sharePhoto(Photo photo);
    }

    public interface Presenter extends com.jc.incodetest.modules.core.Presenter<View> {
        void getPhoto(int id);
        void onShareClicked();
    }

    public interface LocalClient {
        Photo getPhoto(int id);
        void onDestroy();
    }
}
