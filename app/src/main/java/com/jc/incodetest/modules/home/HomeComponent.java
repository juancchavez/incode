package com.jc.incodetest.modules.home;

import com.jc.incodetest.ApplicationComponent;
import com.jc.incodetest.modules.home.data.HomeApiClient;
import com.jc.incodetest.modules.home.data.HomeLocalClient;
import com.jc.incodetest.modules.home.view.HomeActivity;

import dagger.Component;

/**
 * Created by JuanCarlos on 3/12/17.
 */
@HomeScope
@Component(modules = HomeModule.class, dependencies = ApplicationComponent.class)
public interface HomeComponent {
    void injectHomeActivity(HomeActivity homeActivity);
    HomeLocalClient homeLocalClient();
    HomeApiClient homeApiClient();
}
