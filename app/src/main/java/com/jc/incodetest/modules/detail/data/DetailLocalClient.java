package com.jc.incodetest.modules.detail.data;

import com.jc.incodetest.db.RealmManagerContract;
import com.jc.incodetest.model.Photo;
import com.jc.incodetest.modules.detail.DetailContract;

import javax.inject.Inject;

import io.realm.Realm;

/**
 * Created by JC on 3/16/17.
 */

public class DetailLocalClient implements DetailContract.LocalClient {

    private RealmManagerContract mRealmManager;
    private Realm mRealm;

    @Inject
    public DetailLocalClient(RealmManagerContract realmManager) {
        mRealmManager = realmManager;
        mRealm = realmManager.getRealmInstance();
    }

    /**
     * DetailContract.LocalClient methods
     */
    @Override
    public Photo getPhoto(int id) {
        return mRealmManager.getPhoto(mRealm, id);
    }

    @Override
    public void onDestroy() {
        mRealmManager.closeRealm(mRealm);
    }
}
