package com.jc.incodetest.modules.detail;

import javax.inject.Scope;

/**
 * Created by JuanCarlos on 3/12/17.
 */
@Scope
public @interface DetailScope {
}
