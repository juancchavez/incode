package com.jc.incodetest.modules.core;

/**
 * Created by JC on 3/15/17.
 */

public interface PresenterFactory <T extends Presenter> {
    T create();
}
