package com.jc.incodetest;

import android.content.Context;

import com.google.gson.Gson;
import com.jc.incodetest.db.RealmManager;
import com.jc.incodetest.db.RealmManagerContract;
import com.jc.incodetest.network.IncodePhotoService;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by JuanCarlos on 3/12/17.
 */
@ApplicationScope
@Module
public class ApplicationModule {
    private final Context contex;

    public ApplicationModule(Context contex) {
        this.contex = contex;
    }

    @Provides
    @ApplicationScope
    public Context applicationModule() {
        return this.contex;
    }

    @Provides
    @ApplicationScope
    public RealmManagerContract realmManager(Context context) {
        return new RealmManager(context);
    }

    @Provides
    @ApplicationScope
    public Retrofit retrofit() {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .baseUrl("https://photomaton.herokuapp.com")
                .build();
    }

    @Provides
    @ApplicationScope
    public IncodePhotoService photoService(Retrofit retrofit) {
        return retrofit.create(IncodePhotoService.class);
    }
}

