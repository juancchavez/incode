package com.jc.incodetest;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

import io.realm.Realm;

/**
 * Created by JuanCarlos on 3/12/17.
 */

public class IncodeApplication extends Application {
    private ApplicationComponent mComponent;
    private static IncodeApplication mInstance;

    public static IncodeApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        Realm.init(this);
        Fresco.initialize(this);
        mComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getComponent() {
        return mComponent;
    }
}
