package com.jc.incodetest.db;

import com.jc.incodetest.model.Photo;

import java.util.List;

import io.realm.Realm;

/**
 * Created by JC on 3/15/17.
 */

public interface RealmManagerContract {
    Realm getRealmInstance();
    void closeRealm(Realm realm);
    List<Photo> getPhotos(Realm realm);
    Photo getPhoto(Realm realm, int id);
    void savePhotos(Realm realm, List<Photo> photos, Realm.Transaction.OnSuccess onSuccess,
                    Realm.Transaction.OnError onError);
    void savePhoto(Realm realm, Photo photo, Realm.Transaction.OnSuccess onSuccess,
                   Realm.Transaction.OnError onError);
    void clearPhotos(Realm realm);
}
