package com.jc.incodetest.db;

import android.content.Context;

import com.jc.incodetest.model.Photo;

import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by JuanCarlos on 3/12/17.
 */

public class RealmManager implements RealmManagerContract {
    @Inject
    public RealmManager(Context context) {
        Realm.init(context);
    }

    public Realm getRealmInstance() {
        return Realm.getDefaultInstance();
    }

    public void closeRealm(Realm realm) {
        realm.close();
    }

    @Override
    public List<Photo> getPhotos(Realm realm) {
        RealmResults<Photo> realmResults = realm.where(Photo.class).findAllSorted("id", Sort.DESCENDING);
        return realm.copyFromRealm(realmResults);
    }

    @Override
    public Photo getPhoto(Realm realm, int id) {
        return realm.where(Photo.class).equalTo("id", id).findFirst();
    }

    @Override
    public void savePhotos(Realm realm, final List<Photo> photos, Realm.Transaction.OnSuccess onSuccess,
                           Realm.Transaction.OnError onError) {

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                for(Photo photo : photos) {
                    Number maxId = realm.where(Photo.class).max("id");
                    int id = maxId == null ? 0 : maxId.intValue() + 1;
                    photo.setId(id);
                    realm.copyToRealm(photo);
                }
            }
        }, onSuccess, onError);
    }

    @Override
    public void savePhoto(Realm realm, final Photo photo, Realm.Transaction.OnSuccess onSuccess,
                          Realm.Transaction.OnError onError) {

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Number maxId = realm.where(Photo.class).max("id");
                int id = maxId == null ? 0 : maxId.intValue() + 1;
                photo.setId(id);
                realm.copyToRealm(photo);
            }
        }, onSuccess, onError);
    }

    @Override
    public void clearPhotos(Realm realm) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Photo> photos = realm.where(Photo.class).equalTo("isLocalFile", false)
                        .findAll();

                if(!photos.isEmpty()) {
                    photos.deleteAllFromRealm();
                }
            }
        });
    }
}
