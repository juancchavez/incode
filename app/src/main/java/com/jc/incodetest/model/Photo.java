package com.jc.incodetest.model;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by JuanCarlos on 3/12/17.
 */

public class Photo extends RealmObject {
    @PrimaryKey
    private int id;
    private Date publishedAt;
    private String title;
    private String photo;
    private String comment;
    private boolean isLocalFile;

    public Photo(){
    }

    public Photo(boolean isLocalFile, String photo){
        this.isLocalFile = isLocalFile;
        this.photo = photo;
    }

    public int getId() {
        return id;
    }

    public Date getPublishedAt() {
        return publishedAt;
    }

    public String getTitle() {
        return title;
    }

    public String getPhoto() {
        return photo;
    }

    public String getComment() {
        return comment;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPublishedAt(Date publishedAt) {
        this.publishedAt = publishedAt;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isLocalFile() {
        return isLocalFile;
    }

    public void setLocalFile(boolean localFile) {
        isLocalFile = localFile;
    }
}
