package com.jc.incodetest;

import android.content.Context;
import com.jc.incodetest.db.RealmManagerContract;
import com.jc.incodetest.network.IncodePhotoService;

import dagger.Component;

/**
 * Created by JuanCarlos on 3/12/17.
 */
@ApplicationScope
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {
    Context getContext();
    RealmManagerContract getRealmManager();
    IncodePhotoService getPhotoService();
}
